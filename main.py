# This Python file uses the following encoding: utf-8
import subprocess
import sys
import tkinter as tk
import tkinter.filedialog as fd
from tkinter import *
from tkinter.messagebox import showerror, showwarning, showinfo
from tkinter import ttk
import io
import re
import json
from pathlib import Path
import os
from pdfminer.converter import TextConverter
from pdfminer.pdfinterp import PDFPageInterpreter
from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.pdfpage import PDFPage


class App(tk.Tk):
    def __init__(self):
        super().__init__()
        self.wm_title('Sergayka work')
        self.wm_geometry('450x130')
        self.wm_iconbitmap("beautiful.ico")

        container = self.Constructor()
        self.Main_Window(container)

    def Constructor(self):
        container = tk.Frame(self, width=600, height=300)
        container.pack(expand=1)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        return container

    def Main_Window(self, container):
        container.destroy()
        container = self.Constructor()
        btn_json = ttk.Button(container, text="Create Json File", command=lambda: self.Create_json(container))
        bnt_script = ttk.Button(container, text="Start Script", command=lambda: self.Start_Script(container))
        btn_json.grid(row=0, column=0, ipadx=15, ipady=7)
        bnt_script.grid(row=0, column=1, ipadx=15, ipady=7)

    def Create_json(self, container):
        container.destroy()
        container = self.Constructor()

        folder_path = StringVar()
        json_path = StringVar()

        text_folder_path = Label(container, text="Show me the path to PDF")
        text_folder_path.grid(row=0, column=0)
        input_path = Entry(container, textvariable=folder_path)
        input_path.grid(row=0, column=1)
        btnFind = ttk.Button(container, text="Browse folder", command=lambda: self.SetFolderPath(folder_path))
        btnFind.grid(row=0, column=2)

        text_json_path = Label(container, text="Where I can save JSON?")
        text_json_path.grid(row=2, column=0)
        input_path_json = Entry(container, textvariable=json_path)
        input_path_json.grid(row=2, column=1)
        btnFind = ttk.Button(container, text="Browse folder", command=lambda: self.SetJsonFolderPath(json_path))
        btnFind.grid(row=2, column=2)

        btn_save = ttk.Button(container, text="Create", command=lambda: self.Save(folder_path, json_path))
        btn_back = ttk.Button(container, text="Back", command=lambda: self.Main_Window(container))
        btn_save.grid(row=6, column=0, ipadx=15, columnspan=2, pady=7)
        btn_back.grid(row=6, column=1, ipadx=15, columnspan=2, pady=7)

    def Save(self, folder_path, json_path):
        folder_pdf_path = folder_path.get()
        folder_json_path = json_path.get()
        if len(folder_pdf_path) == 0 or len(folder_json_path) == 0:
            showerror(title="Error 404", message="PATH!!?!? TRY AGAIN!")
        else:
            ConvertPDFtoJson(folder_pdf_path, folder_json_path)

    def SetFolderPath(self, folder_path):
        folder_selected = fd.askdirectory()
        folder_path.set(folder_selected)

    def SetJsonFolderPath(self, json_path):
        folder_selected = fd.askdirectory()
        json_path.set(folder_selected)

    def Start_Script(self, container):
        container.destroy()
        container = self.Constructor()

        script_path = StringVar()

        text_script_path = Label(container, text="Find ps1 script")
        text_script_path.grid(row=0, column=0)
        input_path = Entry(container, textvariable=script_path)
        input_path.grid(row=0, column=1)
        btnFind = ttk.Button(container, text="Browse file", command=lambda: self.SetScriptPath(script_path))
        btnFind.grid(row=0, column=2)

        json_path = StringVar()

        text_json_path = Label(container, text="Find JSON file")
        text_json_path.grid(row=2, column=0)
        input_path_json = Entry(container, textvariable=json_path)
        input_path_json.grid(row=2, column=1)
        btnFind = ttk.Button(container, text="Browse file", command=lambda: self.SetJsonPath(json_path))
        btnFind.grid(row=2, column=2)

        btn_start = ttk.Button(container, text="Start", command=lambda: self.Start_script(json_path, script_path))
        btn_start.grid(row=6, column=0, columnspan=2, pady=7)
        btn_back = ttk.Button(container, text="Back", command=lambda: self.Main_Window(container))
        btn_back.grid(row=6, column=1, columnspan=2, pady=7)




    def SetJsonPath(self, json_path):
        file_selected = fd.askopenfilename(filetypes=(("JSON files", "*.json"),))
        json_path.set(file_selected)

    def SetScriptPath(self, script_path):
        file_selected = fd.askopenfilename(filetypes=(("Powershell files", "*.ps1"),))
        script_path.set(file_selected)

    def Start_script(self, json_path, script_path):
        file_script_path = script_path.get()
        file_json_path = json_path.get()
        if len(file_script_path) == 0 or len(file_json_path) == 0:
            showerror(title="Error 404", message="PATH!!?!? TRY AGAIN!")
        else:
            showinfo(title="Уведомление", message="Данный раздел еще не готов")
            # pass
            # file_script_path = file_script_path.replace('/', "\\"*2)
            # subprocess.call(f'powershell.exe -File "{file_script_path}" -jsonContent {file_json_path}',
            #                 stdout=sys.stdout)
            # showinfo(title='Working process', message='Work was completed')

class ConvertPDFtoJson:
    def __init__(self, folder_pdf_path, folder_json_path):
        self.json_path = f"{folder_json_path}/result.json"
        self.list_pdf = folder_pdf_path
        self.Start()

    def Start(self):
        open(self.json_path, "w").close()
        data_file = []
        for pdf in os.listdir(self.list_pdf):
            if Path(pdf).suffix == ".pdf":
                self.ConvertPDFtoText(pdf_path=f'{self.list_pdf}/{pdf}', data_file=data_file)

        self.PushToJson(data_file)
        showinfo(title='Working process', message='Work was completed')

    def ConvertPDFtoText(self, pdf_path, data_file):
        resource_manager = PDFResourceManager()
        fake_file_handle = io.StringIO()
        converter = TextConverter(resource_manager, fake_file_handle)
        page_interpreter = PDFPageInterpreter(resource_manager, converter)

        with open(pdf_path, 'rb') as fh:
            for page in PDFPage.get_pages(fh,
                                          caching=True,
                                          check_extractable=True):
                page_interpreter.process_page(page)

            text = fake_file_handle.getvalue()

        # Закрытие хандлера
        converter.close()
        fake_file_handle.close()

        if text:
            cnils = text[-26:-12]
            inn = text[-38:-26]
            data = []
            for i in range(len(text)):
                if text[i:i + 5] == "Прием" and text[i + 6] != " ":
                    tmp = text[i + 16:-38].split(" ")
                    if len(tmp) == 3:
                        tmp1 = re.split('(?=[А-Я])', tmp[2])
                    else:
                        tmp1 = re.split('(?=[А-Я])', tmp[2])
                        tmp1[2] = tmp1[2] + " " + ' '.join(tmp[3:])
                    period = text[i + 6:i + 16]

                    data.append(tmp[0])
                    data.append(tmp[1])
                    data.append(tmp1[1])
                    data.append(tmp1[2])
                    data.append(inn)
                    data.append(cnils)
                    data.append(period)

                if text[i:i + 10] == "Увольнение":
                    data.clear()

            if len(data) > 0:
                self.JSONContent(data, data_file)

    def JSONContent(self, data, data_file):
        tmp = {
            "FIO": f"{data[0]} {data[1]}",
            "Surname": f"{data[2]}",
            "Job": f"{data[3]}",
            "INN": f"{data[4]}",
            "SNILS": f"{data[5]}",
            "Date_receipt": f"{data[6]}"
        }
        data_file.append(tmp)

        return data_file

    def PushToJson(self, data_file):
        with open(self.json_path, 'w') as file:
            json.dump(data_file, file, indent=4, sort_keys=False, ensure_ascii=False)


if __name__ == '__main__':
    app = App()
    app.mainloop()
